/* React stuff */
import React from 'react';

/* Modules */
import styled from 'styled-components';

/* Utils */
import colors from '../utils/colors';

const DropdownMenuContainer = styled('ul')`

    position: absolute;
    width: 100%;
    border: 1px solid black;
    border-radius: 4px;
    box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
    display: none;

    & li {
        width: 100%;
        list-style: none;

        & .menu-item {
            width: 100%;
            padding: 0.4rem 0.6rem;
            font-size: 0.8rem;
            border-radius: 0;
            border: none;
            outline: none;

            &:hover {
                cursor: pointer;
                background-color: ${colors.gray_dark};
            } 
            
            &:focus {
                background-color: ${colors.gray_dark};
            }
        }

        & .first-item { border-radius: 4px 4px 0 0; }
        & .last-item { border-radius:  0 0 4px 4px; }
    }

`;

export default function DropdownMenu({ setNotesToShow }) {
    return (
        <DropdownMenuContainer id='drop-down-menu-container' >
            <li><button onClick={() => setNotesToShow((noteToShow: string) => [...noteToShow, 'Yellow'])} className='menu-item first-item' >Yellow</button></li>
            <li><button onClick={() => setNotesToShow((noteToShow: string) => [...noteToShow, 'Blue'])} className='menu-item' >Blue</button></li>
            <li><button onClick={() => setNotesToShow((noteToShow: string) => [...noteToShow, 'Green'])} className='menu-item last-item' >Green</button></li>
        </DropdownMenuContainer>
    )
};
