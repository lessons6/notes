/* React stuff */
import React from "react";

/* Componets */
import DropdownMenu from './dropdown-menu';

/* Modules */
import styled from 'styled-components';

/* Utils */
import colors from "../utils/colors";
import { showAndHiddeMenu } from '../utils/functions';

/* Images */
import BrandLogo from '../assets/svgs/post-it-logo.svg';

const HeaderContainer = styled('header')`

  background-color: ${colors.yellow};
  padding: 1rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: fixed;
  width: 100%;
  left: 0;
  top: 0;

  & .drop-down-menu-button {
    height: fit-content;
    padding: 0.4rem 0.8rem;
    font-size: 0.9rem;
    border-radius: 4px;
    border: none;
    box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);

    &:hover {
      cursor: pointer;
    }

  }

  & .drop-down-menu-container {
    position: relative;
    width: fit-content;
  }

  & svg {
    width: 80px;
    height: 80px;
    display: block;
  }

`;

const Header = ({ setNotesToShow }) => (
  <HeaderContainer>

    <BrandLogo />

    <div className='drop-down-menu-container' onClick={() => showAndHiddeMenu('drop-down-menu-container')} >

      <button className='drop-down-menu-button' >
        + New Note
      </button>

      <DropdownMenu
        setNotesToShow={setNotesToShow}
      />

    </div>

  </HeaderContainer>
);

export default Header;
