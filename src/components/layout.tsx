/* React stuff */
import React from "react";

import styled from 'styled-components';

/* Componets */
import Header from "./header";

/* Styles */
import "./layout.css";

const LayoutContainer = styled('div')`

  position: relative;
  padding: 1rem;
  padding-top: 7.5rem;
  height: fit-content;

`;

const Layout = ({ children, setNotesToShow }) => {
  return (
    <LayoutContainer >

      <Header 
        setNotesToShow={setNotesToShow}
      />

      <div>

        <main>{children}</main>
        {/* <footer> </footer> */}

      </div>
    </LayoutContainer>
  )
};

export default Layout;
