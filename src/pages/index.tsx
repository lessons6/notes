/* React stuff */
import React, { useState } from "react";

/* Components */
import Layout from "../components/layout";
import SEO from "../components/seo";

const IndexPage = () => {

  const [notesToShow, setNotesToShow] = useState([]);

  return (
    <Layout
      setNotesToShow={setNotesToShow}
    >

      <SEO title="Home" />
      {notesToShow.map(item => item)}

    </Layout>
  )
};

export default IndexPage;
