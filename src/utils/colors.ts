let colors = {
    yellow: '#f7dd00',
    gray: '#F1F1F1',
    gray_dark: '#e2e1e0',
};

export default colors;
