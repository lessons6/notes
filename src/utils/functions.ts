import { document } from 'browser-monads';

const showAndHiddeMenu = (idName: string) => {

    document.getElementById(idName).style.display = 'block';

    document.onclick = () => {
        document.getElementById(idName).style.display = 'none';
        document.onclick = null;
    };

};

export { showAndHiddeMenu };
